import requests
import facebook
import sys
import api_utils
from tqdm import tqdm           
from tabulate import tabulate   


#test auth
access_token='EAANwo6mPvVABAFgaIrstr0ah9GbDzZBXP6Jd0vZAmVBGMKaNFAs7pgECbUI6IswzR9qQPy7QQDofNzZBDBQtOjAZB4yYZAOZB55gF4e7BRpVMVLI5TowXllqemOeNJlM0VCV0hspsHrVBolhu0EXnwqCZAiUENAFK628rnNMyTNGAZDZD'
graph = facebook.GraphAPI(access_token=access_token, version='3.1')


def get_all_pages(posts):
   allposts=[]
   allposts = posts['data']
   while(1):
      try:
         next_page_url=posts['paging']['next']  
      except KeyError:
         break
      posts = requests.get(next_page_url).json()
      allposts += posts['data']
   return allposts
   


def get_post_likes(post):
   mylikes=[]
   try:
     likes=graph.get_connections(post['id'],"likes")
     mylikes = get_all_pages(likes)
   except:
     pass 
   return mylikes


def get_liked_pages():
   mypages=[]
   pages = graph.get_connections('me',connection_name='likes')
   mypages = get_all_pages(pages)
   return mypages


def get_user_posts():
   myposts=[]

   posts = graph.get_connections('me',connection_name='posts')
   myposts = get_all_pages(posts)
   return myposts


def get_user_details():
   user = graph.get_object('me',fields='name,id,email,education,friends,age_range,birthday')
   return user


def show_table(friends):
    count=0
    friends_table=[]
    
    for friend_name in sorted(friends, key=friends.get, reverse=True):       #top 5 posts likers
        count+=1
   	friends_table.append([friend_name, friends[friend_name]])
   	if count == 5:
            break

    print friends_table	
    return friends_table

def main():
   friends={}
   myposts = api_utils.get_user_posts() #todos posts
   print "%d posts \n\n"%(len(myposts))
   
   
   for post in tqdm(myposts):
      likes = api_utils.get_post_likes(post)
      for like in likes:
         if like['name'] in friends.keys():
            friends[like['name']]+=1
         else:
            friends[like['name']] = 1
    
   show_table(friends)

   return


if __name__ == "__main__":
    print main()